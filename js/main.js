$(document).ready(function () {
  window.app = function () {
    return {
      cars: [],
      loadedPages: [],
      currentCarIndex: 0,
      pageNumber: 1,
      car: [
        {selector: $('#name'), target: 'name'},
        {selector: $('#origin'), target: 'origin'},
        {selector: $('#hourse-power'), target: 'horsepower'},
        {selector: $('#miles-per-galon'), target: 'piles_per_gallon'},
        {selector: $('#accelleration'), target: 'acceleration'},
        {selector: $('#weight'), target: 'weight_in_lbs'},
        {selector: $('#cylinders'), target: 'cylinders'}
      ],
      init() {
        this.getCars(true);
        let self = this;
        $('#next').on('click', function() {
          self.next()
        })
        $('#prev').on('click', function() {
          self.prev()
        })
        return self;
      },
      getCars (init) {
        if (this.loadedPages.includes(this.pageNumber)) {
          return false
        } else {
          $.get("http://localhost:3000/cars", { _page: this.pageNumber }, (data) => {
            this.loadedPages.push(this.pageNumber)
            data.map(car => this.cars.push(car))
            if (init) {
              this.showCar()
            }
          })
        }
      },
      showCar () {
        let car = this.cars[this.currentCarIndex]
        this.car.map(i => {
          i.selector.text(car[i.target])
        })
      },
      next () {
        if (this.currentCarIndex == (this.cars.length - 2)) {
          this.pageNumber++
          this.getCars(false)
        }
        this.currentCarIndex++
        this.showCar()
      },
      prev () {
        if (this.currentCarIndex > 0) {
          this.currentCarIndex--
          this.showCar()
        }
      }
    }
  }().init();
});
